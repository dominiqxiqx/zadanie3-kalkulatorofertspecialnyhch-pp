package zadanie3.kalkulator.ofert;

import java.text.DecimalFormat;

public class Price {
    private float _value;
    
    public Price() {
        this(0);
    }
    public Price(float value) {
        this._value = value;
    }
    
    public float getPrice() {
        return this._value;
    }
    public void setPrice(float value) {
        this._value = value;
    }
    @Override
    public String toString() {
        String str = "";
        
        DecimalFormat dFormat = new DecimalFormat("###.##");
        str += "{Price: " + dFormat.format(this._value) + "}";
        
        return str;
    }
}