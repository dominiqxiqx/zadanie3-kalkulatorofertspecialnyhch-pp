package zadanie3.kalkulator.ofert;
public class Client {
    private String _name;
    private Promotion _promotion;
    
    public Client() {
        this(null);
    }
    public Client(Promotion promotion) {
        this._promotion = promotion;
        this._name = "Jakiś tam klient :)";
    }
    
    public Promotion getPromotion() {
        return this._promotion;
    }
    
    @Override
    public String toString() {
        String str = "";
        
        str += "{" + this._name + ", promotion: " + (this._promotion != null ? "Yes" : "No") + "}";
        
        return str;
    }
}
