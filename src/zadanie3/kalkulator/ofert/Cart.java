package zadanie3.kalkulator.ofert;

import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

public class Cart {
    private Price _price;
    private List<Article> _articles;
    private final Client _client;
    
    public Cart(Client client) {
        this._price = new Price();
        this._articles = new ArrayList<>();
        
        this._client = client;
    }
    
    public float getPrice() {
        return this._price.getPrice();
    }
    public void addArticle(Article art, int ammount) {
        for(int i = 0; i < ammount; i++)
            this.addArticle(art);
    }
    
    @Override
    public String toString() {
        String str = "";
        
         DecimalFormat dFormat = new DecimalFormat("###.##");
        str += "{" + this._client.toString() + " articles:\n";
        for(Article art : this._articles)
            str += "\t" + art.toString() + "\n";
        
        str += "\nTotal price: " + dFormat.format(this._price.getPrice()) + "}";
        
        return str;
    }
    
    private void addArticle(Article art) {
        if(art == null)
            return;
        
        this._articles.add(art);
        if(this._client.getPromotion() == null)
            this._price.setPrice(this.getPrice() + art.getPrice());
        else
            this._price.setPrice(this.getPrice() +
                    art.getPrice() * this._client.getPromotion().getValue());
    }
}
